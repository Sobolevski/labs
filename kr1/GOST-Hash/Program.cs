﻿using System;
using System.IO;

namespace GOST_Hash
{
    class Program
    {
        static void Main(string[] args)
        {
            var message = File.ReadAllBytes("../semion_sobolevski.txt");
            StribogHasher hasher = new StribogHasher();
            var hash = hasher.GetHash(message, true);
            var hashCode = hasher.GetHashStringFormat(message, true);

            File.WriteAllBytes("../semion_sobolevski_HASH", hash);
            File.WriteAllText("../semion_sobolevski_HASH-FORMATTED", hashCode);

            foreach (var byteValue in hash)
            {
                Console.Write(byteValue);
            }

            Console.WriteLine("\n");
            Console.WriteLine(hashCode);
        }
    }
}
