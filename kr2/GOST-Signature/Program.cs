﻿using System;
using System.Text;

namespace GOST_Signtature
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger p = new BigInteger("6277101735386680763835789423207666416083908700390324961279", 10);
            BigInteger a = new BigInteger("-3", 10);
            BigInteger b = new BigInteger("64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1", 16);
            byte[] xG = CustomByteArrayConverter("03188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012");
            BigInteger n = new BigInteger("ffffffffffffffffffffffff99def836146bc9b1b4d22831", 16);
            
            Signature DS = new Signature(p, a, b, n, xG);
            BigInteger d = DS.genPrivateKey(192);
            CurvePoint Q = DS.ModelingPublicKey(d);
            StribogHasher hasher = new StribogHasher();

            var path = "../semion_sobolevski.txt";
            var message = System.IO.File.ReadAllText(path);
            byte[] H = hasher.GetHash(Encoding.Default.GetBytes(message), false);
            string sign = DS.GetSignature(H, d);
            Console.WriteLine("Message sign: {0}", sign);
            path = "../semion_sobolevski_RESULT.txt";
            System.IO.File.WriteAllText(path, sign);

            path = "../semion_sobolevski.txt";
            String message2 = System.IO.File.ReadAllText(path);
            byte[] H2 = hasher.GetHash(Encoding.Default.GetBytes(message2), false);
            path = "../semion_sobolevski_RESULT.txt";
            string signVer= System.IO.File.ReadAllText(path);

            bool result = DS.VerifySignature(H2, signVer, Q);

            if (result)
                Console.WriteLine("Success");
            else
                Console.WriteLine("Wrong");

            Console.ReadLine();
        }

        private static byte[] CustomByteArrayConverter(string input)
        {
            byte[] data = new byte[input.Length / 2];
            string HexByte = string.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                HexByte = input.Substring(i * 2, 2);
                data[i] = Convert.ToByte(HexByte, 16);
            }
            return data;
        }
    }
}
