#include <string>
#include <vector>

#include "rsa.h"

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>


#define _CRT_SECURE_NO_DEPRECATE

using namespace std;

int main() {

	  uint64_t q = 523;
	  uint64_t p = 65537;
	  uint64_t n = p * q;
	  uint64_t t = (p - 1) * (q - 1);
	  uint64_t e = calc_e(t);
	  uint64_t d = calc_d(e, t);

	  cout << "Public key (e,n): {" << e << ", " << n << "}" << endl;
	  cout << "Private key (d,n): {" << d << ", " << n << "}" << endl;

	  ifstream ifs("\semion_sobolevski.txt");
	  string message((istreambuf_iterator<char>(ifs)),
		  (istreambuf_iterator<char>()));

	  int remain = message.size() % 8;
	  if (remain > 0) {
		  int whole = message.size() / 8;
		  message.resize((whole + 1) * 8, 0);
	  }

	  auto msg = vector<uint64_t>(message.begin(), message.end());

	  FILE* inputfile;
	  inputfile = fopen("\semion_sobolevski_CRYPTED_RES.txt", "wb");

	  auto enc = encrypt(msg, e, n);

	  for (const auto& v : enc) {
		  cout << hex << setw(2) << setfill('0')
			  << static_cast<unsigned long long int>(v) << " ";
	  }

	  for (size_t i = 0; i < enc.size(); i++) {
		  fwrite(&enc[i], 1, 1, inputfile);
	  }
	  fclose(inputfile);
	  inputfile = fopen("\semion_sobolevski_DECRYPTED_RES.txt", "wb");

	  auto dec = decrypt(enc, d, n);

	  for (size_t i = 0; i < dec.size(); i++) {
		  fwrite(&dec[i], 1, 1, inputfile);
	  }
	  fclose(inputfile);
	  return 0;
}
