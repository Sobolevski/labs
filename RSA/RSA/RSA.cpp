#include "rsa.h"

uint64_t gcd(uint64_t a, uint64_t b) {
  if (a < b) {
    uint64_t t = a;
    a = b;
    b = t;
  }

  while (true) {
    uint64_t r = a % b;
    if (r == 0)
      return b;
    a = b;
    b = r;
  }
}

bool is_prime(uint64_t n) {
  uint64_t root = static_cast<uint64_t>(floor(sqrt(static_cast<long double>(n))));

  for (uint64_t i = 2; i <= root; ++i) {
    if (n % i == 0)
      return false;
  }

  return true;
}

uint64_t calc_e(uint64_t t) {
  for (uint64_t e = 2; e < t; e++) {
    if (gcd(e, t) == 1) {
      return e;
    }
  }

  return -1;
}

uint64_t calc_d(uint64_t e, uint64_t t) {
  uint64_t k = 1;
  while (true) {
    k += t;
    if (k % e == 0) {
      return k / e;
    }
  }
}

vector<uint64_t> encrypt(const std::vector<uint64_t> &msg, uint64_t e, uint64_t n) {
  vector<uint64_t> enc(msg.size(), 0);
  for (size_t k = 0; k < msg.size(); ++k) {
    uint64_t res = msg[k];
    for (uint64_t i = 0; i < e - 1; ++i) {
      res *= msg[k];
      res %= n;
    }
    enc[k] = res;
  }
  return enc;
}


uint64_t modPow(uint64_t b, uint64_t e, uint64_t m) {
	if (m == 1) {
		return 0;
	}
	else {
		uint64_t r = 1;
		b = b % m;
		while (e > 0) {
			if (e % 2 == 1) {
				r = (r * b) % m;
			}
			e = e >> 1;
			b = (b * b) % m;
		}
		return r;
	}
}


vector<uint64_t> decrypt(const std::vector<uint64_t> &enc, uint64_t d, uint64_t n) {
  vector<uint64_t> dec(enc.size(), 0);
  for (size_t k = 0; k < enc.size(); ++k) {
	  uint64_t res = modPow(enc[k], d, n);
	  dec[k] = res;
  }
  return dec;
}
