#include <algorithm>
#include <cassert>
#include <cstring>
#include <iterator>

#include "headers/block.h"
#include "headers/cipher.h"
#include "headers/const.h"

using namespace std;

DESCipher::DESCipher(const std::vector<uint8_t> &key) {
  assert(key.size() == 8);

  this->generate_subkeys(key);
}

const vector<uint64_t> &DESCipher::GetSubkeys() const {
  return this->subkeys;
}

vector<uint8_t> DESCipher::Encrypt(const std::vector<uint8_t> &msg) {
  return this->crypt(msg, true);
}

vector<uint8_t> DESCipher::Decrypt(const std::vector<uint8_t> &msg) {
  return this->crypt(msg, false);
}

vector<uint8_t> DESCipher::crypt(const std::vector<uint8_t> &msg,
                                      bool encrypt) {
  assert(msg.size() > 0 && msg.size() % blockSize == 0);

  size_t blocks = msg.size() / blockSize;

  vector<uint8_t> res(msg.size());

  vector<uint8_t> resBuffer(blockSize);
  vector<uint8_t> msgBuffer(blockSize);

  for (size_t i = 0; i < blocks; i++) {
    fill(resBuffer.begin(), resBuffer.end(), 0);
    copy(msg.begin() + i * blockSize, msg.begin() + (i + 1) * blockSize,
              msgBuffer.begin());

    if (encrypt)
      this->encrypt(resBuffer, msgBuffer);
    else
      this->decrypt(resBuffer, msgBuffer);

    copy(resBuffer.begin(), resBuffer.end(), res.begin() + i * blockSize);
  }
  return res;
}

void DESCipher::generate_subkeys(const std::vector<uint8_t> &keyBytes) {
  if (!feistelBoxInited) {
    init_feistel_box();
    feistelBoxInited = true;
  }

  uint64_t key = 0;
  for (int i = 0; i < 8; i++) {
    key += (static_cast<uint64_t>(keyBytes[i]) << blockSize * (7 - i));
  }

  uint64_t permutedKey = permute_block(key, permuted_choice_1);

  vector<uint32_t> leftRotations =
      ks_rotate(static_cast<uint32_t>(permutedKey >> 28));
  vector<uint32_t> rightRotations =
      ks_rotate(static_cast<uint32_t>(permutedKey << 4) >> 4);

  for (int8_t i = 0; i < 16; i++) {
    uint64_t pc2Input = static_cast<uint64_t>(leftRotations[i]) << 28 |
                        static_cast<uint64_t>(rightRotations[i]);
    this->subkeys[i] = unpack(permute_block(pc2Input, permuted_choice_2));
  }
}

void DESCipher::encrypt(std::vector<uint8_t> &dst,
                        const std::vector<uint8_t> &src) {
  assert(src.size() >= blockSize);

  assert(dst.size() >= blockSize);

  encrypt_block(this->subkeys, dst, src);
}

void DESCipher::decrypt(std::vector<uint8_t> &dst,
                        const std::vector<uint8_t> &src) {
  assert(src.size() >= blockSize);

  assert(dst.size() >= blockSize);

  decrypt_block(this->subkeys, dst, src);
}
