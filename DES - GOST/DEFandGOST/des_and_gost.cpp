﻿#include <iostream>
#include <cstdint>
#include <string>

#include "headers/block.h"
#include "headers/triple_des.h"
#include <vector>
#include <fstream>
#include <string>
#define _CRT_SECURE_NO_DEPRECATE

using namespace std;


vector<uint8_t> prompt_key(int len = 8) {
	string key;
	cout << "Enter key:" << std::endl;
	getline(cin, key);
	key.resize(len, 0);
	return vector<uint8_t>(key.begin(), key.end());
}

vector<uint8_t> prompt_msg() {
	ifstream ifs("\semion_sobolevski.txt");
	string msg((istreambuf_iterator<char>(ifs)),
		(istreambuf_iterator<char>()));

	int remain = msg.size() % 8;
	if (remain > 0) {
		int whole = msg.size() / 8;
		msg.resize((whole + 1) * 8, 0);
	}

	return vector<uint8_t>(msg.begin(), msg.end());
}


unsigned int Sbox[8][16] =
{
	{4,10,9,2,13,8,0,14,6,11,1,12,7,15,5,3},
	{14,11,4,12,6,13,15,10,2,3,8,1,0,7,5,9},
	{5,8,1,13,10,3,4,2,14,15,12,7,6,0,9,11},
	{7,13,10,1,0,8,9,15,14,4,6,12,11,2,5,3},
	{6,12,7,1,5,15,13,8,4,10,9,14,0,3,11,2},
	{4,11,10,0,7,2,1,13,3,6,8,5,9,12,15,14},
	{13,11,4,1,3,15,5,9,0,10,14,7,6,8,2,12},
	{1,15,13,0,5,7,10,4,9,2,3,14,6,11,8,12}
};

//Default value, table value
char secretkey[33] = "98765432109876543210987654321098";

void GOST_28147(bool mode, vector<uint8_t> key_uint)
{	
	string key_str;
	key_str.assign(key_uint.begin(), key_uint.end());
	strcpy(secretkey, key_str.c_str());

	unsigned int key[8];

	for (int i = 0; i < 8; i++)
	{
		key[i] = (secretkey[4 * i] << 24) + (secretkey[1 + 4 * i] << 16) + (secretkey[2 + 4 * i] << 8) + secretkey[3 + 4 * i];  // Складывание каждых 4-х символов ключа в одно число

		key[i] =   
			((key[i] & 0x1) << 31) + ((key[i] & 0x2) << 29) + ((key[i] & 0x4) << 27) + ((key[i] & 0x8) << 25) +
			((key[i] & 0x10) << 23) + ((key[i] & 0x20) << 21) + ((key[i] & 0x40) << 19) + ((key[i] & 0x80) << 17) +
			((key[i] & 0x100) << 15) + ((key[i] & 0x200) << 13) + ((key[i] & 0x400) << 11) + ((key[i] & 0x800) << 9) +
			((key[i] & 0x1000) << 7) + ((key[i] & 0x2000) << 5) + ((key[i] & 0x4000) << 3) + ((key[i] & 0x8000) << 1) +
			((key[i] & 0x10000) >> 1) + ((key[i] & 0x20000) >> 3) + ((key[i] & 0x40000) >> 5) + ((key[i] & 0x80000) >> 7) +
			((key[i] & 0x100000) >> 9) + ((key[i] & 0x200000) >> 11) + ((key[i] & 0x400000) >> 13) + ((key[i] & 0x800000) >> 15) +
			((key[i] & 0x1000000) >> 17) + ((key[i] & 0x2000000) >> 19) + ((key[i] & 0x4000000) >> 21) + ((key[i] & 0x8000000) >> 23) +
			((key[i] & 0x10000000) >> 25) + ((key[i] & 0x20000000) >> 27) + ((key[i] & 0x40000000) >> 29) + ((key[i] & 0x80000000) >> 31);
	}

	FILE * inputfile;            
	FILE * outputfile;
	int fsize;
	inputfile = NULL;
	outputfile = NULL;
	if (!mode)
	{	
		inputfile = fopen("\semion_sobolevski.txt", "rb");

		fseek(inputfile, 0, SEEK_END);         // Вычисление размера файла
		fsize = ftell(inputfile);
		fseek(inputfile, 0, SEEK_SET);

		outputfile = fopen("\semion_sobolevski_CRYPTED_GOST.txt", "wb");
	}
	if (mode)
	{
		inputfile = fopen("\semion_sobolevski_CRYPTED_GOST.txt", "rb");

		fseek(inputfile, 0, SEEK_END);         // Вычисление размера файла
		fsize = ftell(inputfile);
		fseek(inputfile, 0, SEEK_SET);

		outputfile = fopen("\semion_sobolevski_DECRYPTED_GOST.txt", "wb");
	}

	while (fsize)
	{
		unsigned int A = 0;              
		unsigned int B = 0;             

		for (int i = 0; i < 4; i++)     
		{
			if (fsize)
			{
				unsigned int s = 0;
				fread(&s, 1, 1, inputfile);

				A += (s << (24 - (i * 8)));        
				fsize--;
			}
			else A += (32 << (24 - (i * 8)));  
		}                                
	
		for (int i = 0; i < 4; i++)   
		{
			if (fsize)
			{
				unsigned int s = 0;
				fread(&s, 1, 1, inputfile);

				B += (s << (24 - (i * 8))); 
				fsize--;
			}
			else B += (32 << (24 - (i * 8)));    
		}          

		A =    
			((A & 0x1) << 31) + ((A & 0x2) << 29) + ((A & 0x4) << 27) + ((A & 0x8) << 25) +
			((A & 0x10) << 23) + ((A & 0x20) << 21) + ((A & 0x40) << 19) + ((A & 0x80) << 17) +
			((A & 0x100) << 15) + ((A & 0x200) << 13) + ((A & 0x400) << 11) + ((A & 0x800) << 9) +
			((A & 0x1000) << 7) + ((A & 0x2000) << 5) + ((A & 0x4000) << 3) + ((A & 0x8000) << 1) +
			((A & 0x10000) >> 1) + ((A & 0x20000) >> 3) + ((A & 0x40000) >> 5) + ((A & 0x80000) >> 7) +
			((A & 0x100000) >> 9) + ((A & 0x200000) >> 11) + ((A & 0x400000) >> 13) + ((A & 0x800000) >> 15) +
			((A & 0x1000000) >> 17) + ((A & 0x2000000) >> 19) + ((A & 0x4000000) >> 21) + ((A & 0x8000000) >> 23) +
			((A & 0x10000000) >> 25) + ((A & 0x20000000) >> 27) + ((A & 0x40000000) >> 29) + ((A & 0x80000000) >> 31);

		B = 
			((B & 0x1) << 31) + ((B & 0x2) << 29) + ((B & 0x4) << 27) + ((B & 0x8) << 25) +
			((B & 0x10) << 23) + ((B & 0x20) << 21) + ((B & 0x40) << 19) + ((B & 0x80) << 17) +
			((B & 0x100) << 15) + ((B & 0x200) << 13) + ((B & 0x400) << 11) + ((B & 0x800) << 9) +
			((B & 0x1000) << 7) + ((B & 0x2000) << 5) + ((B & 0x4000) << 3) + ((B & 0x8000) << 1) +
			((B & 0x10000) >> 1) + ((B & 0x20000) >> 3) + ((B & 0x40000) >> 5) + ((B & 0x80000) >> 7) +
			((B & 0x100000) >> 9) + ((B & 0x200000) >> 11) + ((B & 0x400000) >> 13) + ((B & 0x800000) >> 15) +
			((B & 0x1000000) >> 17) + ((B & 0x2000000) >> 19) + ((B & 0x4000000) >> 21) + ((B & 0x8000000) >> 23) +
			((B & 0x10000000) >> 25) + ((B & 0x20000000) >> 27) + ((B & 0x40000000) >> 29) + ((B & 0x80000000) >> 31);

		unsigned int T;
		for (int i = 0; i < 32; i++)
		{
			T = 0;
			if (!mode)
			{
				if (i < 24) T = (A + key[i % 8]) % 0x100000000;   
				else T = (A + key[7 - (i % 8)]) % 0x100000000;
			}
			if (mode)
			{
				if (i < 8) T = (A + key[i % 8]) % 0x100000000; 
				else T = (A + key[7 - (i % 8)]) % 0x100000000;
			}

			unsigned int Fragments[8] =  
			{
				(T & 0xF0000000) >> 28,
				(T & 0xF000000) >> 24,
				(T & 0xF00000) >> 20,
				(T & 0xF0000) >> 16,
				(T & 0xF000) >> 12,
				(T & 0xF00) >> 8,
				(T & 0xF0) >> 4,
				(T & 0xF)
			};

			for (int j = 0; j < 8; j++)
			{
				Fragments[j] = Sbox[j][Fragments[j]]; 
			}

			T = (Fragments[0] << 28) +    
				(Fragments[1] << 24) +
				(Fragments[2] << 20) +
				(Fragments[3] << 16) +
				(Fragments[4] << 12) +
				(Fragments[5] << 8) +
				(Fragments[6] << 4) +
				Fragments[7];

			T = (T << 11) | (T >> 21);           
			T ^= B;                

			if (i != 31)
			{
				B = A;         
				A = T;
			}
			else
			{
				B = T;
			}
		}
		A =               
			((A & 0x1) << 31) + ((A & 0x2) << 29) + ((A & 0x4) << 27) + ((A & 0x8) << 25) +
			((A & 0x10) << 23) + ((A & 0x20) << 21) + ((A & 0x40) << 19) + ((A & 0x80) << 17) +
			((A & 0x100) << 15) + ((A & 0x200) << 13) + ((A & 0x400) << 11) + ((A & 0x800) << 9) +
			((A & 0x1000) << 7) + ((A & 0x2000) << 5) + ((A & 0x4000) << 3) + ((A & 0x8000) << 1) +
			((A & 0x10000) >> 1) + ((A & 0x20000) >> 3) + ((A & 0x40000) >> 5) + ((A & 0x80000) >> 7) +
			((A & 0x100000) >> 9) + ((A & 0x200000) >> 11) + ((A & 0x400000) >> 13) + ((A & 0x800000) >> 15) +
			((A & 0x1000000) >> 17) + ((A & 0x2000000) >> 19) + ((A & 0x4000000) >> 21) + ((A & 0x8000000) >> 23) +
			((A & 0x10000000) >> 25) + ((A & 0x20000000) >> 27) + ((A & 0x40000000) >> 29) + ((A & 0x80000000) >> 31);

		B =  
			((B & 0x1) << 31) + ((B & 0x2) << 29) + ((B & 0x4) << 27) + ((B & 0x8) << 25) +
			((B & 0x10) << 23) + ((B & 0x20) << 21) + ((B & 0x40) << 19) + ((B & 0x80) << 17) +
			((B & 0x100) << 15) + ((B & 0x200) << 13) + ((B & 0x400) << 11) + ((B & 0x800) << 9) +
			((B & 0x1000) << 7) + ((B & 0x2000) << 5) + ((B & 0x4000) << 3) + ((B & 0x8000) << 1) +
			((B & 0x10000) >> 1) + ((B & 0x20000) >> 3) + ((B & 0x40000) >> 5) + ((B & 0x80000) >> 7) +
			((B & 0x100000) >> 9) + ((B & 0x200000) >> 11) + ((B & 0x400000) >> 13) + ((B & 0x800000) >> 15) +
			((B & 0x1000000) >> 17) + ((B & 0x2000000) >> 19) + ((B & 0x4000000) >> 21) + ((B & 0x8000000) >> 23) +
			((B & 0x10000000) >> 25) + ((B & 0x20000000) >> 27) + ((B & 0x40000000) >> 29) + ((B & 0x80000000) >> 31);

		T = (A & 0xFF000000) >> 24;   fwrite(&T, 1, 1, outputfile);
		T = (A & 0xFF0000) >> 16;     fwrite(&T, 1, 1, outputfile);
		T = (A & 0xFF00) >> 8;        fwrite(&T, 1, 1, outputfile);
		T = (A & 0xFF);             fwrite(&T, 1, 1, outputfile);

		T = (B & 0xFF000000) >> 24;   fwrite(&T, 1, 1, outputfile);
		T = (B & 0xFF0000) >> 16;     fwrite(&T, 1, 1, outputfile);
		T = (B & 0xFF00) >> 8;        fwrite(&T, 1, 1, outputfile);
		T = (B & 0xFF);             fwrite(&T, 1, 1, outputfile);
	}  
	fclose(inputfile);
	fclose(outputfile);
}

int main()
{
	vector<uint8_t> key = prompt_key(24);

	GOST_28147(false, key);
	GOST_28147(true, key);

	vector<uint8_t> src = prompt_msg();

	TripleDES dc(key);

	FILE* inputfile;
	inputfile = fopen("\semion_sobolevski_CRYPTED_DES.txt", "wb");
	vector<uint8_t> dst1 = dc.Encrypt_3DES(src);

	for (size_t i = 0; i < dst1.size(); i++) {
		fwrite(&dst1[i], 1, 1, inputfile);
	}
	fclose(inputfile);
	inputfile = fopen("\semion_sobolevski_DECRYPTED_DES.txt", "wb");

	vector<uint8_t> dst2 = dc.Decrypt_3DES(dst1);

	for (size_t i = 0; i < dst2.size(); i++) {
		fwrite(&dst2[i], 1, 1, inputfile);
	}
	
	return 0;
}