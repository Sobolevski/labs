#include <vector> 

using namespace std;

class DESCipher {
  const uint8_t blockSize = 8;
  vector<uint64_t> subkeys = vector<uint64_t>(16);

  void generate_subkeys(const vector<uint8_t> &keyBytes);

  vector<uint8_t> crypt(const vector<uint8_t> &msg, bool encrypt);

  void encrypt(vector<uint8_t> &dst, const vector<uint8_t> &src);
  void decrypt(vector<uint8_t> &dst, const vector<uint8_t> &src);

public:
  DESCipher(const vector<uint8_t> &key);

  const vector<uint64_t> &GetSubkeys() const;

  vector<uint8_t> Encrypt(const vector<uint8_t> &msg);
  vector<uint8_t> Decrypt(const vector<uint8_t> &msg);
};
